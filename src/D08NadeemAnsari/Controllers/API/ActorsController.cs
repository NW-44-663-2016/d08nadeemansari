using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08NadeemAnsari.Models;

namespace D08NadeemAnsari.Controllers
{
    [Produces("application/json")]
    [Route("api/Actors")]
    public class ActorsController : Controller
    {
        private AppDbContext _context;

        public ActorsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Actors
        [HttpGet]
        public IEnumerable<Actor> GetActors()
        {
            return _context.Actors;
        }

        // GET: api/Actors/5
        [HttpGet("{id}", Name = "GetActor")]
        public IActionResult GetActor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Actor actor = _context.Actors.Single(m => m.ActorID == id);

            if (actor == null)
            {
                return HttpNotFound();
            }

            return Ok(actor);
        }

        // PUT: api/Actors/5
        [HttpPut("{id}")]
        public IActionResult PutActor(int id, [FromBody] Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != actor.ActorID)
            {
                return HttpBadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Actors
        [HttpPost]
        public IActionResult PostActor([FromBody] Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Actors.Add(actor);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ActorExists(actor.ActorID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetActor", new { id = actor.ActorID }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public IActionResult DeleteActor(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Actor actor = _context.Actors.Single(m => m.ActorID == id);
            if (actor == null)
            {
                return HttpNotFound();
            }

            _context.Actors.Remove(actor);
            _context.SaveChanges();

            return Ok(actor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Count(e => e.ActorID == id) > 0;
        }
    }
}