using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08NadeemAnsari.Models;

namespace D08NadeemAnsari.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160302043120_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08NadeemAnsari.Models.Actor", b =>
                {
                    b.Property<int>("ActorID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country")
                        .IsRequired();

                    b.Property<int>("HousesOwned");

                    b.Property<int>("LocationID");

                    b.Property<int>("MoviesActedIn");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<double>("NetWorth");

                    b.Property<string>("State")
                        .IsRequired();

                    b.HasKey("ActorID");
                });

            modelBuilder.Entity("D08NadeemAnsari.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country")
                        .IsRequired();

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place")
                        .IsRequired();

                    b.Property<string>("State")
                        .IsRequired();

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08NadeemAnsari.Models.Actor", b =>
                {
                    b.HasOne("D08NadeemAnsari.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
