﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08NadeemAnsari.Models
{
    public class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
           
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            
            if(context.Locations.Any())
            {
                return;
            }
            if (context.Actors.Any())
            {
                return;
            }

            var loc1 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var loc2 = context.Locations.Add(new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" });
            var loc3 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var loc4 = context.Locations.Add(new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" });
            var loc5 = context.Locations.Add(new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad",State="Telangana", Country = "India" });
            var loc6 = context.Locations.Add(new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", State = "Telangana", Country = "India" });
            var loc7 = context.Locations.Add(new Location() { Latitude = 36.8019385, Longitude = 44.6347775, Place = "Kent", State = "Telangana", Country = "USA" });

            

            context.Actors.AddRange(new Actor() { Name = "Indra", Country = "India", State = "Telangana", NetWorth = 60000, HousesOwned = 5, LocationID=loc1.Entity.LocationID},
            new Actor() { Name = "Hitler", Country = "5", State = "ExtremeAction", NetWorth = 799999, HousesOwned = 7, LocationID = loc2.Entity.LocationID },
            new Actor() { Name = "NapoleanBonaparte", Country = "5", State = "Action", NetWorth = 799999, HousesOwned = 9, LocationID = loc3.Entity.LocationID },
            new Actor() { Name = "Troy", Country = "5", State = "Action", NetWorth = 7999, HousesOwned = 12, LocationID = loc4.Entity.LocationID },
            new Actor() { Name = "Eragon", Country = "4", State = "Adventure", NetWorth = 799999989, HousesOwned = 13, LocationID = loc5.Entity.LocationID },
            new Actor() { Name = "8Mile", Country = "5", State = "Biography", NetWorth = 79999989, HousesOwned = 4, LocationID = loc6.Entity.LocationID },
            new Actor() { Name = "Jobs", Country = "5", State = "Biography", NetWorth = 79999, HousesOwned = 4, LocationID = loc7.Entity.LocationID });

            context.SaveChanges();
            }

        }
    
    }

