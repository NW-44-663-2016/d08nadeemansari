﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.Data.Entity.ChangeTracking;

namespace D08NadeemAnsari.Models
{
    public class Location
    {
        [ScaffoldColumn(false)]
        public int LocationID { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Place { get; set; }
        [Required]
        public string State { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

      
    }
}
