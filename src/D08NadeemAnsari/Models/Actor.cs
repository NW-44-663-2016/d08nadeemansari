﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace D08NadeemAnsari.Models
{
    public class Actor
    {
        [ScaffoldColumn(false)]
        public int ActorID { get; set; }
        [Required]
        [Display(Name="Actor Name")]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        [Display(Name= "Net Worth")]
        public double NetWorth { get; set; }
        [Required]
        [Range(5, 100)]
        [Display(Name = "Movies Acted In")]
        public int MoviesActedIn { get; set; }
        [Display(Name = "Houses Owned")]
        public int HousesOwned { get; set; }

        [ScaffoldColumn(false)]
        public int LocationID { get; set; }

        public virtual Location Location { get; set; }
    }
}
